const Course = require("../models/Course");

// Create a new course
		/*
			Steps:
			1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
			2. Save the new User to the database
		*/

module.exports.addCourse = (reqBody, isAdmin) => {

		const newCourse = null;

		if (isAdmin) {
		  newCourse = new Course({
		    name: reqBody.name,
		    description: reqBody.description,
		    price: reqBody.price
		  });
		}
		return Promise.resolve(newCourse).then(course => course ? course.save() : false);

}

	
// Ms. Janie's solution for s39 courseController
// 	let newCourse = new Course ({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	})


// 	return newCourse.save().then(course => true).catch(err => false);
// }


// Retrieve all courses
		/*
			Steps:
			1. Retrieve all the courses from the database
		*/
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => result)
		.catch(err => err);
}


module.exports.getAllActive = () => {

	return Course.find({ isActive : true }).then(result => result).catch(err => err)
};


module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result
	}).catch(err => err)
};

// Update a course
		/*
			Steps:
			1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
			2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
		*/
		// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		Syntax:
			ModelName.findByIdAndUpdate(documentId, updatesToBeApplied).then(statement)

			req.params = { courseId: 6406f6a58c46ae11f13f31cb }
			req.params.courseId = 6406f6a58c46ae11f13f31cb
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(course => true).catch(err => err);

}


// failed s40 solutions
// module.exports.archiveCourse = (reqParams, reqBody) => {

//   return Course.findByIdAndUpdate(reqParams, reqBody, {isActive: false}, { new: true }).then(updatedCourse => false).catch(err => console.error(err));
// };


// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
//Session 40 - Activity Solution 2
module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.courseId, updateActiveField).then(course => true).catch(err => err);
};
